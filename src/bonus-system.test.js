import {calculateBonuses} from "./bonus-system.js";

describe('Bonus system tests', () => {
    test('Valid bonus calculation',  () => {
        expect(calculateBonuses("Standard", 1)).toBeCloseTo(0.05, 5);
        expect(calculateBonuses("Standard", 10000)).toBeCloseTo(0.05 * 1.5, 5);
        expect(calculateBonuses("Standard", 50000)).toBeCloseTo(0.05 * 2, 5);
        expect(calculateBonuses("Standard", 100000)).toBeCloseTo(0.05 * 2.5, 5);

        expect(calculateBonuses("Premium", 1)).toBeCloseTo(0.1, 5);
        expect(calculateBonuses("Premium", 10000)).toBeCloseTo(0.1 * 1.5, 5);
        expect(calculateBonuses("Premium", 50000)).toBeCloseTo(0.1 * 2, 5);
        expect(calculateBonuses("Premium", 100000)).toBeCloseTo(0.1 * 2.5, 5);

        expect(calculateBonuses("Diamond", 1)).toBeCloseTo(0.2, 5);
        expect(calculateBonuses("Diamond", 10000)).toBeCloseTo(0.2 * 1.5, 5);
        expect(calculateBonuses("Diamond", 50000)).toBeCloseTo(0.2 * 2, 5);
        expect(calculateBonuses("Diamond", 100000)).toBeCloseTo(0.2 * 2.5, 5);

        expect(calculateBonuses(" ", 1)).toBeCloseTo(0, 5);
        expect(calculateBonuses(" ", 10000)).toBeCloseTo(0, 5);
        expect(calculateBonuses(" ", 50000)).toBeCloseTo(0, 5);
        expect(calculateBonuses(" ", 100000)).toBeCloseTo(0, 5);
    });
});